package hu.dpc.edu.concurrency;

import java.util.Random;

public class Producer implements Runnable {

    private Random random = new Random();

    private MyStack stack;
    private ShutdownManager shutdownManager;

    public Producer(MyStack stack, ShutdownManager shutdownManager) {
        this.stack = stack;
        this.shutdownManager = shutdownManager;
    }

    @Override
    public void run() {
        try {

            final String tn = Thread.currentThread().getName();

            for (int i = 0; i < 50; i++) {
                Thread.sleep(random.nextInt(100));

                char c = (char) (random.nextInt('Z' - 'A') + 'A');

                try {
                    stack.push(c);
                } catch (StackFullException ex) {
                    System.out.println("Stack is full");
                }

                if (shutdownManager.isTimeToShutdown()) {
                    System.out.println(tn + " Shutting down...");
                    break;
                }

            }

        } catch (InterruptedException ex) {
            System.out.println("Producer thread was interrupted, terminating...");
        }


    }
}
