package hu.dpc.edu.concurrency;

public class StackFullException extends RuntimeException {
    public StackFullException() {
    }

    public StackFullException(String message) {
        super(message);
    }

    public StackFullException(String message, Throwable cause) {
        super(message, cause);
    }

    public StackFullException(Throwable cause) {
        super(cause);
    }

    public StackFullException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
