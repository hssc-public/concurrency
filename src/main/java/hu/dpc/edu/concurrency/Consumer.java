package hu.dpc.edu.concurrency;

import java.util.NoSuchElementException;
import java.util.Random;

public class Consumer implements Runnable {

    private Random random = new Random();

    private MyStack stack;

    private ShutdownManager shutdownManager;

    public Consumer(MyStack stack, ShutdownManager shutdownManager) {
        this.stack = stack;
        this.shutdownManager = shutdownManager;
    }

    @Override
    public void run() {
        try {

            final String tn = Thread.currentThread().getName();

            for (int i = 0; i < 50; i++) {
                Thread.sleep(random.nextInt(100));
                try {
                    final char popped = stack.pop();
                } catch (NoSuchElementException ex) {
                    System.out.println("Stack is empty");
                }
                if (shutdownManager.isTimeToShutdown()) {
                    System.out.println(tn + " Shutting down...");
                    break;
                }
            }
        } catch (InterruptedException ex) {
            System.out.println("Consumer thread was interrupted, shutting down...");
        }

    }
}
