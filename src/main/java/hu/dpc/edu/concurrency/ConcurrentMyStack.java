package hu.dpc.edu.concurrency;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ConcurrentMyStack implements MyStack {

    private char[] array = new char[10];
    private volatile int index = 0;

    private final Lock LOCK = new ReentrantLock();

    private final Condition NOT_EMPTY = LOCK.newCondition();

    private final Condition NOT_FULL = LOCK.newCondition();

    @Override
    public void push(char c) throws InterruptedException {
        LOCK.lock();
        try {
            final String tn = Thread.currentThread().getName();
            System.out.println(tn + " push(" + c + ")");
            while (index == array.length) {
                System.out.println(tn + " WAITING... (stack is full)");
                NOT_FULL.await();
            }
            NOT_EMPTY.signal();
            array[index++] = c;
        } finally {
            LOCK.unlock();
        }
    }

    @Override
    public char pop() throws InterruptedException {
        LOCK.lock();
        try {
            final String tn = Thread.currentThread().getName();
            System.out.println(tn + " pop()");
            while (index == 0) {
                System.out.println(tn + " WAITING...");
                NOT_EMPTY.await();

                if (index == 0) {
                    System.out.println(tn + " FALSE NOTIFICATION!!!!!!!!!!!!");
                }
            }

            NOT_FULL.signal();

            final char c = array[--index];
            System.out.println(tn + " popped: " + c);
            return c;
        } finally {
            LOCK.unlock();
        }
    }
}
