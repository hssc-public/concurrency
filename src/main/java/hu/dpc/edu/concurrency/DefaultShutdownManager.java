package hu.dpc.edu.concurrency;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;

public class DefaultShutdownManager implements ShutdownManager{

    private boolean timeToShutDown;

    private List<Runnable>shutdownTasks = new ArrayList<>();

    @Override
    public void shutdown() {
        System.out.println("Shutting down...");
        timeToShutDown = true;
        shutdownTasks.forEach(Runnable::run);
    }

    @Override
    public void addWorkerThread(Thread thread) {
        shutdownTasks.add(() -> thread.interrupt());
    }

    @Override
    public void addExecutorService(ExecutorService executorService) {
        shutdownTasks.add(() -> executorService.shutdown());
    }

    @Override
    public boolean isTimeToShutdown() {
        return false;
    }
}
