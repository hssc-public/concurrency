package hu.dpc.edu.concurrency;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class CallableDemo {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        final ExecutorService executorService = Executors.newFixedThreadPool(3);

        MyTask alma = new MyTask("Alma", 1000);
        MyTask korte = new MyTask("Korte", 2000);
        MyTask banan = new MyTask("Banan", 2000);
        MyTask szolo = new MyTask("Szolo", 1000);

        final Future<String> almaFuture = executorService.submit(alma);
        final Future<String> korteFuture = executorService.submit(korte);
        final Future<String> bananFuture = executorService.submit(banan);
        final Future<String> szoloFuture = executorService.submit(szolo);

        System.out.println("after submitting...");

        final String almaResult = almaFuture.get();
        System.out.println("almaFuture.get(): " + almaResult);

        System.out.println("cancelling banan");
        bananFuture.cancel(true);

        executorService.shutdown();

    }
}
