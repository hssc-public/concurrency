package hu.dpc.edu.concurrency;

public interface MyStack {
    void push(char c) throws InterruptedException;

    char pop() throws InterruptedException;
}
