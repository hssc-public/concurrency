package hu.dpc.edu.concurrency;

import java.util.Random;

public class MyRunnable implements Runnable {

    private int i;
    private Random random = new Random();

    @Override
    public void run() {

        final String tn = Thread.currentThread().getName();

        while (i < 50) {
            System.out.println(tn + ": i: " + i);
            try {
                Thread.sleep(random.nextInt(100));
            } catch (InterruptedException e) {
                throw new RuntimeException("Thread was interrupted", e);
            }
            i++;
        }
    }
}
