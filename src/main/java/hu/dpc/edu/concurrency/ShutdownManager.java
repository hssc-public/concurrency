package hu.dpc.edu.concurrency;

import java.util.concurrent.ExecutorService;

public interface ShutdownManager {
    void shutdown();

    void addWorkerThread(Thread thread);

    void addExecutorService(ExecutorService executorService);

    boolean isTimeToShutdown();
}
