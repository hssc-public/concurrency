package hu.dpc.edu.concurrency;

import java.util.concurrent.Callable;

public class MyTask implements Callable<String> {

    private String name;

    private long waitFor;

    public MyTask(String name, long waitFor) {
        this.name = name;
        this.waitFor = waitFor;
    }

    @Override
    public String call() throws Exception {
        final String tn = Thread.currentThread().getName();
        try {
            System.out.println(tn + " Task is called, working on " + name + "...");
            Thread.sleep(waitFor);
            System.out.println(tn + " Task is done, returning 'processed: " + name + "'...");

            return "processed: " + name;
        } catch (InterruptedException ex) {
            System.out.println(tn + " interrupted task " + name);
            throw ex;
        }
    }
}
