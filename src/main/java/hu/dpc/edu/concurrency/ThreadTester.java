package hu.dpc.edu.concurrency;

public class ThreadTester {
    public static void main(String[] args) throws InterruptedException {
        final MyRunnable myRunnable = new MyRunnable();

        final Thread t1 = new Thread(myRunnable, "T1");
        final Thread t2 = new Thread(myRunnable, "T2");
        t1.join();

        System.out.println(t1.isAlive());
        System.out.println(t2.isAlive());
        System.out.println("Starting threads...");
        t1.start();
        t2.start();
        System.out.println(t1.isAlive());
        System.out.println(t2.isAlive());
        System.out.println("main ends");

        t2.join();
        System.out.println("After t2.join()");

        System.out.println("t1" + t1.isAlive());
        System.out.println("t2" + t2.isAlive());
    }
}
